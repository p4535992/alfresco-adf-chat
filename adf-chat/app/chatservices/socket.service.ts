import { Injectable } from '@angular/core';
/// <reference path="../typings/main/ambient/atmosphere/index.d.ts" />

import * as Rx from 'rxjs';
import * as atmosphere from 'atmosphere.js';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class SocketService {

    public eventsSubject: Rx.Subject<any> = new Rx.Subject<any>();
    private userLoggedIn: boolean = false;
    private userName: String = null;
    private socket: atmosphere.Atmosphere;
    private subSocket: any;
    private request: atmosphere.Request;
    public dataStream: Subject<any> = new Subject<any>();

    setConfig(url: String, username: String, config?: atmosphere.Request) {

        this.request = config || <atmosphere.Request>{
            url: url,
            contentType: 'application/json',
            logLevel: 'debug',
            transport: 'websocket',
            fallbackTransport: 'long-polling',
            onOpen: (response: atmosphere.Response) => {
                console.log('Atmosphere connected using ' + response.transport);
                if (username !== '') {
                    console.log('call for user list');
                    this.send({ author: username });
                }
            },
            onMessage: (response: atmosphere.Response) => {
                this.dataStream.next(response);
            },
            onClose: (response: atmosphere.Response) => {
                this.dataStream.complete();
            },
            onError: (response: atmosphere.Response) => {
                this.dataStream.error(response);
            }
        };

        this.subSocket = atmosphere.subscribe(this.request);
    }
    public socketUnsubscribe() {
        atmosphere.unsubscribe();
    }
    public send(data: any) {
        this.subSocket.push(atmosphere.util.stringifyJSON(data));
    }

}
