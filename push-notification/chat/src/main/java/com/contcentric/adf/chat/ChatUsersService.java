package com.contcentric.adf.chat;

import static org.atmosphere.cpr.ApplicationConfig.MAX_INACTIVE;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.Heartbeat;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResourceFactory;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class creates service which would provide push notifications containing
 * online users list and messages
 * 
 * @author Shubham Rajput
 * @version 1.0
 */

@ManagedService(path = "/chat/users", atmosphereConfig = MAX_INACTIVE + "=120000")
public class ChatUsersService {

	private final Logger logger = LoggerFactory.getLogger(ChatUsersService.class);

	@Inject
	private BroadcasterFactory factory;

	@Inject
	@Named("/chat/users")
	private Broadcaster broadcaster;

	@Inject
	private AtmosphereResource atmosphereResource;

	@Inject
	private AtmosphereResourceEvent event;

	@Inject
	private AtmosphereResourceFactory resourceFactory;

	@Heartbeat
	public void onHeartbeat(final AtmosphereResourceEvent event) {
		logger.trace("Heartbeat send by {}", event.getResource());
	}

	/**
	 * Invoked when the connection has been fully established and suspended,
	 * that is, ready for receiving messages.
	 *
	 */
	@Ready
	public void onReady() {
		logger.info("Browser {} connected", atmosphereResource.uuid());
		logger.info("BroadcasterFactory used {}", factory.getClass().getName());
		logger.info("Broadcaster injected {}", broadcaster.getID());
	}

	/**
	 * Invoked when the client disconnects or when the underlying connection is
	 * closed unexpectedly.
	 *
	 */
	@Disconnect
	public void onDisconnect() {
		if (event.isCancelled()) {
			logger.info("Browser {} unexpectedly disconnected", event.getResource().uuid());
		} else if (event.isClosedByClient()) {
			logger.info("Browser {} closed the connection", event.getResource().uuid());
		}
		StoreUsers.getStoredUsers().getUsers().values().remove(event.getResource().uuid());
		Set<String> users = StoreUsers.getStoredUsers().getUsers().keySet();
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			event.broadcaster().broadcast(objectMapper.writeValueAsString(new ResponseUsers(users)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is called when user becomes online or offline and sends
	 * messages
	 * 
	 * @param message
	 * @throws IOException
	 */
	@org.atmosphere.config.service.Message(encoders = { JacksonEncoder.class }, decoders = { JacksonDecoder.class })
	public void onMessage(Message message) throws IOException {
		logger.info("{} just sent {}", message.getAuthor(), message.getMessage());

		Broadcaster privateChannel = factory.lookup("/private", true);
		ObjectMapper om = new ObjectMapper();
		if (message.getTo() == null || "".equals(message.getTo())) {

			// Add user to users map
			if (isNotNull(message.getAuthor())) {
				StoreUsers.getStoredUsers().getUsers().put(message.getAuthor(), atmosphereResource.uuid());
			}
			Set<String> users = StoreUsers.getStoredUsers().getUsers().keySet();
			for (String key : StoreUsers.getStoredUsers().getUsers().keySet()) {

				AtmosphereResource ar = resourceFactory.find(StoreUsers.getStoredUsers().getUsers().get(key));
				privateChannel.addAtmosphereResource(ar);
			}

			// Broadcast json containing users list to all users
			privateChannel.broadcast(om.writeValueAsString(new ResponseUsers(users)));

		} else {

			if (isNotNull(message.getAuthor()) && isNotNull(message.getTo())) {

				logger.info("To.." + message.getTo());
				AtmosphereResource toResource = resourceFactory
						.find(StoreUsers.getStoredUsers().getUsers().get(message.getTo()));

				logger.info("Sending private message...");
				if (storeMessage(message)) {
					privateChannel.broadcast(om.writeValueAsString(new ResponseUsers(message.getAuthor(),
							message.getMessage(), message.getTo(), message.getDate())), toResource);
				}
			}

		}
	}

	/**
	 * Invoked to store conversation messages to alfresco repository
	 * 
	 * @param message
	 * @return
	 * @throws IOException
	 */
	private boolean storeMessage(Message message) throws IOException {

		HttpURLConnection conn = null;

		logger.info("Storing Message to ECM Host " + LoadProperties.getPropertyValue("alfresco-host"));
		String host = LoadProperties.getPropertyValue("alfresco-host");
		String port = LoadProperties.getPropertyValue("alfresco-port");
		boolean result = false;
		try {
			URL url = new URL(host + ":" + port + "/alfresco/s/chat/storechat");
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("authorization", message.getTicket().trim());
			conn.setDoOutput(true);
			ObjectMapper om = new ObjectMapper();
			String input = om.writeValueAsString(
					new ResponseUsers(message.getAuthor(), message.getMessage(), message.getTo(), message.getDate()));

			logger.info("Request Body " + input);
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());

			}

			result = true;

		} catch (Exception e) {

			if (e instanceof IOException) {

				throw new IOException();
			} else if (e instanceof MalformedURLException) {

				throw new MalformedURLException();
			}

		} finally {

			conn.disconnect();
		}
		return result;
	}

	/**
	 * Check null object
	 *
	 * @param ob
	 * @return
	 */
	private boolean isNotNull(Object ob) {

		if (ob == null || "".equals(ob)) {

			return false;
		}

		return true;

	}

}
