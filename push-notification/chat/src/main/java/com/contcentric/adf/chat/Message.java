package com.contcentric.adf.chat;

/**
 * @author Shubham Rajput
 *
 */
public class Message {

	private String message;
	private String author;
	private String to;
	private long date;
	private String ticket;

	public Message() {
		this("", "");
	}

	public Message(String author, String message) {
		this.author = author;
		this.message = message;
	}

	public Message(String author, String message, String to, long date) {
		this.author = author;
		this.message = message;
		this.to = to;
		this.date = date;
	}

	public String getMessage() {
		return message;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long time) {
		this.date = time;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

}
