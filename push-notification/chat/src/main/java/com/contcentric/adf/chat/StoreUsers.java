package com.contcentric.adf.chat;

import java.util.HashMap;
import java.util.Map;

/**
 * Singleton class to store online users
 * 
 * @author Shubham Rajput
 *
 */
public class StoreUsers {

	private Map<String, String> users = new HashMap<>();

	private static StoreUsers storeUsers;

	private StoreUsers() {
	}

	public static StoreUsers getStoredUsers() {

		if (storeUsers == null) {

			storeUsers = new StoreUsers();
		}
		return storeUsers;
	}

	public Map<String, String> getUsers() {

		return storeUsers.users;
	}

}
