/**
 * Parameters from the clients
 * 
 * to : user at the other end
 * message : content of the message
 * date : time-stamp of the message
 */
var to = json.get("to");
var message = json.get("message");
var date = json.get("date");

//Global variables to use across all the functions
var contentType = "conv:chatStore";
var msgFrom = person.properties.userName;
var chatFolderName = to + '-' + msgFrom;
var revChatFolderName = msgFrom + '-' + to;
var messagesPerFile = 100; // Number of messages per conversation file
var currentConvFolder = getCurrentConvFolder();
var currentConvFile = getCurrentConvFile(currentConvFolder);
storeMessages(currentConvFile);

/**
 * Retrieves user conversations folder to store the chat
 * @returns chatFolder
 */
function getCurrentConvFolder() {
    var chatFolder;
    if (companyhome.childByNamePath("Conversations/" + chatFolderName) != null) {
	chatFolder = companyhome.childByNamePath("Conversations/"
		+ chatFolderName);
    } else if (companyhome
	    .childByNamePath("Conversations/" + revChatFolderName) != null) {
	chatFolder = companyhome.childByNamePath("Conversations/"
		+ revChatFolderName);
    } else {
	chatFolder = companyhome.childByNamePath("Conversations").createFolder(
		revChatFolderName);
	chatFolder.setPermission("Collaborator", to);
    }
    return chatFolder;
}

/**
 * To get latest file from the list of all conversations
 * between users e.g. there may be 15 files, each having
 * fixed number of messages. So it chooses the latest
 * file using 'isCurrent' property out of all files
 * @param chatFolder
 * @returns convFile 
 */
function getCurrentConvFile(chatFolder) {
    var children = chatFolder.childFileFolders();
    var convFile;
    var currFile;
    for (var i = 0; i < children.length; i++) {
	if (children[i].properties["conv:isCurrent"] == true) {
	    convFile = children[i];
	}
    }

    if (children.length > 0) {
	currFile = chatFolder.createFile(chatFolder.getName() + "-"
		+ children.length, contentType);
    } else {
	currFile = chatFolder.createFile(chatFolder.getName(), contentType);
    }

    currFile.properties["conv:startDate"] = date;
    currFile.properties["conv:isCurrent"] = true;
    currFile.setPermission("Collaborator", to);
    currFile.content = '{"messages":[]}';
    currFile.save();
    convFile = currFile;
    return convFile;
}

/**
 * Stores message into the file and sets time-stamp
 * of the message and other details as the metadata
 * of a file.
 * @param convFile
 * @returns 
 */
function storeMessages(convFile) {

    var dateArray = convFile.properties["conv:dateTime"];
    var from = convFile.properties["conv:from"];
    if (dateArray != null && dateArray.length >= messagesPerFile) {

	logger.log("inside new file creation");
	convFile.properties["conv:isCurrent"] = false;
	convFile.save();
	convFile = getCurrentConvFile(convFile.getParent());
	dateArray = convFile.properties["conv:dateTime"];
	from = convFile.properties["conv:from"];
    }

    if (dateArray == null) {
	dateArray = new Array();
	dateArray.push(date);
	convFile.properties["conv:dateTime"] = dateArray;
    } else {
	dateArray.push(date);
	convFile.properties["conv:dateTime"] = dateArray;
    }

    if (from == null) {
	from = new Array();
	from.push(msgFrom);
	convFile.properties["conv:from"] = from;
    } else {
	from.push(msgFrom);
	convFile.properties["conv:from"] = from;
    }

    var obj = JSON.parse(convFile.getContent());
    obj.messages.push(message);
    convFile.content = jsonUtils.toJSONString(obj);
    convFile.save();
}
