/**
 * Parameters from the ADF client
 * 
 * startDate : starting date of the conversations 
 * endDate : ending date of the conversations
 * to : the other user involved in the conversations
 */
var startDate = args["startDate"];
var endDate = args["endDate"];
var to = args["with"];

var contentType = "conv:chatStore";
var from = person.properties.userName;
var documentName = to + '-' + from;
var chatFolderName = to + '-' + from;
var revDocumentName = from + '-' + to;

var userTo;
var rootFolder;
var chatFolder;

/**
 * To verify whether the 'Conversations' folder exists at the company home or
 * not
 * isRootFolderExist = true, if Conversations folder exits else false
 * 
 * @returns isRootFolderExist
 */
function isRootFolder() {
    var rootChatFolder = companyhome.childByNamePath("Conversations");
    var isRootFolderExist;
    if (!rootChatFolder) {
	isRootFolderExist = false;
	
    } else {
	rootFolder = rootChatFolder.properties["cm:name"].toString();
	isRootFolderExist = true;
	
    }
    
    return isRootFolderExist;
}

/**
 * To verify the users' chat folder is exist or not
 * isExist = true, if chat folder exists else false
 *
 * @returns isExist 
 *  
 */
function isChatFolder() {

    var children = companyhome.childByNamePath("Conversations").getChildren();
    var isExist = false;

    for (var index = 0; index < children.length; index++) {

	if (children[index].properties["cm:name"] == chatFolderName) {
	    chatFolder = children[index].properties["cm:name"].toString();
	    isExist = true;
	    
	} else if (children[index].properties["cm:name"] == revDocumentName) {
	    chatFolder = children[index].properties["cm:name"].toString();
	    isExist = true;
	    
	}
    }
    return isExist;
}

/**
 * Accepts range of dates and returns old conversations
 * @param startDate
 * @param endDate
 * @returns msg in the JSON format
 */
function getOldMessages(startDate, endDate) {

    var messages = {};
    var msg = [];

    if (isRootFolder() && isChatFolder()) {

	var chatDocList = companyhome.childByNamePath(
		rootFolder + "/" + chatFolder).getChildren();

	for (var index = 0; index < chatDocList.length; index++) {

	    var msgContent = JSON.parse(chatDocList[index].getContent()).messages;
	    var msgFrom = chatDocList[index].properties["conv:from"];
	    var fileEntities = chatDocList[index].getName().split("-");
	    var dateArray = new Array();

	    dateArray = chatDocList[index].properties["conv:dateTime"];

	    for (var i = 0; i < dateArray.length; i++) {
	    
		var msgTo;
		if (fileEntities[0].toString() == msgFrom[i].toString()) {
		    msgTo = fileEntities[1];
		    
		} else if (fileEntities[1] == msgFrom[i]) {
		    msgTo = fileEntities[0];
		    
		}

		// Filtering messages by date
		var inRange = dateArray[i] >= startDate
			&& dateArray[i] < endDate;
		if (inRange) {
		    msg.push({
			date : dateArray[i],
			message : msgContent[i].toString(),
			from : msgFrom[i].toString(),
			to : msgTo.toString()
		    });
		}
	    }
	    messages.msg = msg;
	    if (chatDocList[index].properties["conv:startDate"] === startDate
		    && chatDocList[index].properties["conv:endDate"] === endDate) {
		messages = JSON.parse(chatDocList[index].getContent()).messages;
		
	    }
	}
    }
    return jsonUtils.toJSONString(msg);
}

model.messages = getOldMessages(startDate, endDate).toString();